import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';

export default class Namecard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      objectsArray: [
        {id:1,  name: "User A", email:"usera@gmail.com", phone:"093123456", posittion:"Web Developer", address:"#27,Str 2014, Tek Tla, Phnome Penh", image:"https://images.unsplash.com/photo-1518806118471-f28b20a1d79d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"},
        {id:2,  name: "User B", email:"userb@gmail.com", phone:"094123456", posittion:"Web Developer", address:"#27,Str 2014, Tek Tla, Phnome Penh", image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQzp3AH2Ph_yLxlfpimAg8DWCupYRgbq7pp6OAlqH1kwZaETLK0"} ,
        {id:3,  name: "User C", email:"userc@gmail.com", phone:"094123456", posittion:"Web Developer", address:"#27,Str 2014, Tek Tla, Phnome Penh", image:"http://www.sarkarinaukrisearch.in/wp-content/uploads/2019/01/cute-profile-pic-for-whatsa.jpg"} ,
        {id:4,  name: "User D", email:"userd@gmail.com", phone:"010123456", posittion:"Web Developer", address:"#27,Str 2014, Tek Tla, Phnome Penh", image:"https://i.pinimg.com/originals/6b/13/7a/6b137ad5b69b0c778b55e1dc9d86fef2.jpg"} ,
        {id:5,  name: "User E", email:"usere@gmail.com", phone:"010123456", posittion:"Web Developer", address:"#27,Str 2014, Tek Tla, Phnome Penh", image:"https://i.pinimg.com/originals/ad/86/00/ad86000ae7f18e5ee5589c66a99edf00.jpg"} ,
        {id:6,  name: "User F", email:"userf@gmail.com", phone:"010123456", posittion:"Web Developer", address:"#27,Str 2014, Tek Tla, Phnome Penh", image:"https://cosmic-s3.imgix.net/43170b80-11b4-11e9-81dc-0f3169ce52b7-55149558-stock-vector-profile-view-of-sad-bearded-man-wearing-hat-looking-down.jpg"} ,
        {id:8,  name: "User G", email:"userg@gmail.com", phone:"010123456", posittion:"Web Developer", address:"#27,Str 2014, Tek Tla, Phnome Penh", image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrwXdQf6FMF3W3rU09aQRu6eJsNj3QHiAvQ12YTWSLcgBMSJmkZw"} ,
        {id:9,  name: "User H", email:"userh@gmail.com", phone:"010123456", posittion:"Web Developer", address:"#27,Str 2014, Tek Tla, Phnome Penh", image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0yzCqnhEKj8B43ryrzhP61BADX7rpuufOyFuvJhA7TBxl37a3"} ,
        {id:10, name: "User I", email:"useri@gmail.com", phone:"010123456", posittion:"Web Developer", address:"#27,Str 2014, Tek Tla, Phnome Penh", image:"https://i.dailymail.co.uk/i/pix/2017/04/20/13/3F6B966D00000578-4428630-image-m-80_1492690622006.jpg"} ,
        {id:11, name: "User J", email:"userj@gmail.com", phone:"010123456", posittion:"Web Developer", address:"#27,Str 2014, Tek Tla, Phnome Penh", image:"http://www.latestseotutorial.com/wp-content/uploads/2017/02/latestfunnydppics-300x274.jpg"},
      ]
    };
  }

  renderItem = ({item}) => {
    return (
      <TouchableOpacity>
        <View style={styles.profile}>
          <Image source={{ uri: item.image }} style={styles.profile_pic} />
          <View>
            <View style={styles.nameProfile}>
              <Text style={styles.nameTxt} numberOfLines={1} ellipsizeMode="tail">{item.name}</Text>
              <Text style={styles.mobileNumber}>{item.phone}</Text>
            </View>
            <View> 
              <Text style={styles.maillTxt}>{item.posittion}</Text>
            </View>
              <Text style={styles.maillTxt}>{item.email}</Text>
            <View> 
            </View>
            <View style={styles.addressContainer}>
              <Text style={styles.addressStyle}>{item.address}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return(
      <View style={{ flex: 1, marginTop: 50 }} >
        <FlatList 
          extraData={this.state}
          data={this.state.objectsArray}
          keyExtractor = {(item) => {
            return item.id.toString();
          }}
          renderItem={this.renderItem}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  profile: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#DCDCDC',
    backgroundColor: '#0995bc',
    borderBottomWidth: 1,
    padding: 10,
  },
  profile_pic: {
    borderRadius: 30,
    width: 60,
    height: 60,
  },
  nameProfile: {
   flexDirection: 'row',
    justifyContent: 'space-between',
    width: 280,
  },
  nameTxt: {
    marginLeft: 15,
    fontWeight: '600',
    color: '#fff',
    fontSize: 18,
    width:170,
  },
  mobileNumber: {
    fontWeight: '600',
    color: '#fff',
    fontSize: 14,
  },
  addressContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  addressStyle: {
    fontWeight: '400',
    color: '#fff',
    fontSize: 14,
    marginLeft: 15,
  },
  maillTxt: {
    marginLeft: 15,
    fontWeight: '400',
    color: '#fff',
    fontSize: 14,
    width:170,
  }
}); 